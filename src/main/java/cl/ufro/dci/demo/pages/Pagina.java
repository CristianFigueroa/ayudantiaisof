package cl.ufro.dci.demo.pages;

public class Pagina {
    public static final String HOME = "home";
    public static final String PERSONAS = "personas";
    public static final String FORMULARIO_NUEVA_PERSONA = "nuevaPersona";
}