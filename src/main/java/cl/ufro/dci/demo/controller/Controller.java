package cl.ufro.dci.demo.controller;

import cl.ufro.dci.demo.model.Persona;
import cl.ufro.dci.demo.model.dat.PersonaRepository;
import cl.ufro.dci.demo.pages.Pagina;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
@RequestMapping("")
public class Controller {

    PersonaRepository personaRepository = PersonaRepository.obtenerInstancia();

    @GetMapping(path = {"/home",""})
    public String viewHome(Model model) {
        return Pagina.HOME;
    }

    @GetMapping(path = {"/agregarPersona"})
    public ModelAndView viewAgregarPersona() {
        return new ModelAndView(Pagina.FORMULARIO_NUEVA_PERSONA).addObject("persona",new Persona());
    }

     @PostMapping("/agregarPersona")
     public String añadirNuevaPersona(Persona persona, Model model) { // post es el objeto que recibo desde el formulario
        Persona personaAux = new Persona(persona.getNombre(), persona.getApellido(),persona.getRut());
        personaRepository.addPersona(personaAux);
         System.out.println(personaRepository.getPersonaList());
        return Pagina.HOME;
     }

    @GetMapping(path = {"/personas"})
    public ModelAndView viewListaPersonas() {
        return new ModelAndView(Pagina.PERSONAS).addObject("personas",personaRepository.getPersonaList());
    }

    @GetMapping(path = {"/post"})
    public String eliminarPersona(
            //@requestParam para variables de post?id=2
            //@PathVariable para variables de post/2
            @RequestParam(name = "rut", required = true) int rut
    ) { //parametro por url
        personaRepository.viewPersona(0);
        personaRepository.removePersona(0);
        return Pagina.PERSONAS;
    }

}
