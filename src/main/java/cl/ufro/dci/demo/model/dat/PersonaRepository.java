package cl.ufro.dci.demo.model.dat;

import cl.ufro.dci.demo.model.Persona;

import java.util.ArrayList;
import java.util.List;

public class PersonaRepository {

    private List<Persona> personaList;

    private static PersonaRepository personaRepository;

    //Metodo para obtener instancia de la clase
    public static PersonaRepository obtenerInstancia(){
        // return Optional.of(HumanoRepository).orElse(new HumanoRepository());
        if (personaRepository == null){
            personaRepository = new PersonaRepository();
        }
        return personaRepository;
    }

    private PersonaRepository(){
        this.personaList = new ArrayList<>();
    }

    public List<Persona> getPersonaList(){
        return personaList;
    }

    public void addPersona(Persona persona){
        this.personaList.add(persona);
    }

    public void editPersona(int id, Persona persona){
        this.personaList.set(id,persona);
    }

    public void removePersona(int id){
        this.personaList.remove(id);
    }

    public Persona viewPersona(int id){
        return this.personaList.get(id);
    }
}